## Proyectos y Recursos del Curso de Python - Pyladies La Paz

**Pyladies La Paz** Repositorio del curso 

### TECNOLOGÍAS

  * [Django](https://www.djangoproject.com/)
  * [Python](https://www.djangoproject.com/)
  

### REDES SOCIALES

  * [Facebook](https://www.facebook.com/pyladieslapaz/)
  * [Meetup](https://goo.gl/mpjoCc)
  * [Twitter](https://twitter.com/PyLadiesLaPaz)
  * [Whatsapp](https://chat.whatsapp.com/LVF59C0Qndi8LGE9gpVIZL)
  * [GitHub](https://github.com/pythonlp)

### Como levantar el proyecto

  * virtualenv -p python3 env
  * python manage.py runserver
  * ./ngrok http 8000


https://449771be.ngrok.io/messenger/webhook

### PYLADIES LA PAZ

<p align="center"><img src="https://goo.gl/1BNGyE"></p>
